from typing import Any, Generic, List, Optional, Type, TypeVar
from fastapi.encoders import jsonable_encoder
from sqlalchemy.orm import Session
from pydantic import BaseModel

from db.session import Base as BaseSession


ModelType = TypeVar('ModelType', bound=BaseSession)
CreateSchemaType = TypeVar('CreateSchemaType', bound=BaseModel)
UpdateSchemaType = TypeVar('UpdateSchemaType', bound=BaseModel)


class Base(Generic[ModelType, CreateSchemaType]):
    def __init__(self, model: Type[ModelType]):
        self.model = model

    def get(self, db: Session, id: Any) -> Optional[ModelType]:
        return db.query(self.model).filter(self.model.id == id).first()

    def get_all(self, db: Session, *, skip: int = 0, limit: int = 100) -> List[ModelType]:
        self.db.query(self.model).offset(skip).limit(limit).all()
    
    def create(self, db: Session, *, object: CreateSchemaType) -> ModelType: 
        data = jsonable_encoder(object)
        db_object = self.model(**data)
        db.add(db_object)
        db.commit()
        db.refresh(db_object)
        return db_object
