from typing import List, Optional
from fastapi.encoders import jsonable_encoder
from sqlalchemy.orm import Session


from .base import Base
from schemas.tag import TagBase, TagDisplay
from models.tag import Tag


class TagService(Base[Tag, TagBase]):
    def create(self, db: Session, *, object: TagBase) -> Tag:
        data = jsonable_encoder(object)
        db_object = self.model(**data)
        db.add(db_object)
        db.commit()
        db.refresh(db_object)
        return db_object
    
    def get_tags(self, db: Session, *, skip: int = 0, limit: int = 100) -> List[Tag]:
        return db.query(self.model).offset(skip).limit(limit).all()
    
    def get_tag(self, db: Session, id: int) -> Optional[Tag]:
        return db.query(self.model).filter(Tag.id == id).first()


tag = TagService(Tag)
