import os
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import create_engine

from core.config import settings

basedir = os.path.abspath(os.path.dirname(__file__))


engine = create_engine(f"sqlite:///./database.db", connect_args={"check_same_thread": False})
# engine = create_engine(f"sqlite:///{os.path.join(basedir, 'database.db')}", connect_args={"check_same_thread": False})
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

Base = declarative_base()