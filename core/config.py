import os
import secrets

from pydantic.class_validators import validator
from pydantic import BaseSettings
from typing import Any, Dict, Optional




class Settings(BaseSettings):
    API_V1: str = '/api/v1'
    SECRET_KEY = secrets.token_urlsafe(32)
    # SQLALCHEMY_DATABASE_URI: str = f"sqlite:///{os.path.join(basedir, 'database.db')}"
    PROJECT_NAME: str = 'Recipes Api'


settings = Settings()