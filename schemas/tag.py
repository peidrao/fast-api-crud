from typing import Optional
from pydantic import BaseModel


class TagBase(BaseModel):
    title: Optional[str] = None
    slug: Optional[str] = None
    user: Optional[str] = None
    is_active: Optional[bool] = None

class TagCreate(BaseModel):
    title: str
    slug: str
    user: str


class TagDisplay(BaseModel):
    title: Optional[str] = None
    slug: Optional[str] = None
    user: Optional[str] = None
    is_active: Optional[bool] = None

    class Config:
        orm_mode = True
    