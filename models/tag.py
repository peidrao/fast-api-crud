from sqlalchemy import Column, Integer, String, Boolean
from sqlalchemy.orm import relationships

from db.session import Base


class Tag(Base):
    __tablename__ = 'tag'
    id = Column(Integer, primary_key=True, index=True)
    title = Column(String, index=True)
    slug = Column(String, index=True)
    user = Column(String, index=True)
    is_active = Column(Boolean, index=True)

