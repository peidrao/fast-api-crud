from fastapi import APIRouter

from routers import tag


api_router = APIRouter()
api_router.include_router(tag.router, prefix='/tags', tags=['tags'])