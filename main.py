from fastapi import FastAPI

from api.api import  api_router
from core.config import settings
from db.session import Base, engine


Base.metadata.create_all(bind=engine)

app = FastAPI(title=settings.PROJECT_NAME, openapi_url=f'{settings.API_V1}/openapi.json')

app.include_router(api_router, prefix=settings.API_V1)
