from typing import Any, List
from fastapi import APIRouter, Depends, HTTPException, status
from sqlalchemy.orm import Session


from schemas.tag import TagDisplay, TagCreate
from services.tag import tag
from api.deps import get_db


router = APIRouter()


@router.post('/', response_model=TagDisplay)
def create_tag(*, db: Session = Depends(get_db), tag_obj: TagCreate) -> Any:
    obj = tag.create(db=db, object=tag_obj)
    return obj


@router.get('/', response_model=TagDisplay)
def get_tags(db: Session = Depends(get_db), skip: int = 0, limit: int = 100) -> Any:
    return tag.get_all(db, skip=skip, limit=limit)


@router.get('/{tag_id}', response_model=TagDisplay)
def get_tag_by_id(tag_id: int, db: Session = Depends(get_db)) -> Any:
    object = tag.get(db, tag_id)

    if not object:
        raise HTTPException(status=status.HTTP_404_NOT_FOUND, detail='The tag not found!')
    
    return object
